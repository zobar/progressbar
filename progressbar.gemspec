# -*- encoding: utf-8 -*-
require File.expand_path("../lib/progressbar", __FILE__)

Gem::Specification.new do |s|
  s.name        = "progressbar_zobar"
  s.version     = ProgressBar::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Satoru Takabayashi", "Jose Peleteiro", 'David P Kleinschmidt']
  s.email       = ["satoru@0xcc.net", "jose@peleteiro.net", 'david@kleinschmidt.name']
  s.homepage    = "http://bitbucket.org/zobar/progressbar"
  s.summary     = 'Text progress bar library for Ruby, forked from progressbar.'
  s.description = "Fork of progressbar that allows changing the progress bar's title and total while still in progress."

  s.required_rubygems_version = ">= 1.3.6"

  s.add_development_dependency "bundler", ">= 1.0.0"
  s.add_development_dependency "minitest", ">= 0"
  s.add_development_dependency "yard", ">= 0"
  s.add_development_dependency "rake", ">= 0"
  s.add_development_dependency "simplecov", ">= 0.3.5"
  s.add_development_dependency "bluecloth", ">= 0.3.5"

  s.files        = `git ls-files`.split("\n")
  s.executables  = `git ls-files`.split("\n").map{|f| f =~ /^bin\/(.*)/ ? $1 : nil}.compact
  s.require_path = 'lib'

  s.rdoc_options = ["--charset=UTF-8"]
end
